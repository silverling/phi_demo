#include "phi_a.hpp"
#include "phi_b.hpp"
#include <iostream>

int main() {
  std::cout << "Success: " << func_a() << ' ' << func_b() << std::endl;
  return 0;
}